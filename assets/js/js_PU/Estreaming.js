var getUrl = window.location;
var baseUrl = getUrl.protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1] + "/";

$(document).on('ready', function () {

    chat();

    Recargar_ventana();
    Navegacion();
});
function chat() {
    var enviado = false;
    var mensajes;
    var mensaje_enviado;
    var altura = $('#contenido-chat').prop("scrollHeight");
    $('#contenido-chat').scrollTop(altura);
    $('#enviar_mensajes').bind('keyup', function (e) {
        if (e.keyCode === 13) {
            if ($('#enviar_mensajes').val().trim()!== ''){

                if (enviado === false) {
                    enviado = true;

                    mensaje_enviado=$('#enviar_mensajes').val();
                    $('#enviar_mensajes').val("");
                    addVentanaChatContenido(mensaje_enviado);
                }
            }
        }
    });

    function addVentanaChatContenido(mensaje_enviado) {


        var data = {
            'mensaje': mensaje_enviado,
            'tag': 'chat'
        };
        $.ajax({
            url: baseUrl + "agregar_mensaje",
            type: "post",
            data: data,
            cache: false,
            async:false
        }).done(function (info) {
            
            enviado = false;
        });
    }

}

function Recargar_ventana() {
    var html = "";
    var oldScroll = $('#contenido-chat').prop("scrollHeight");
    var newScroll=0;
    setInterval(function () {

        $.ajax({
            url: baseUrl + "ver_mensajes",
            cache: false
        }).done(function (info) {

            mensajes = JSON.parse(info);
            for (var i in mensajes) {
               
                html += "<div class='usuario'><p >"
                     + mensajes[i].m_chat_usuario + ":</p></div>\n <div class='mensaje'><p>" + mensajes[i].chat_mensaje + "</p></div>";
                
            }
            $('#contenido-chat').html(html);
            newScroll = $('#contenido-chat').prop("scrollHeight");
            if(newScroll>oldScroll){
                $('#contenido-chat').scrollTop(newScroll);
                oldScroll=$('#contenido-chat').prop("scrollHeight");
            }
            html = "";
        });
    }, 2000);
}
function Navegacion(){
    $( "#btnpregunta" ).click(function() {
      $( "#Pregunta" ).toggle();
    });
}