
var codigo;

$(document).on('ready', function () {

    init();
    

});


function init() {
    
    integrantes_directivas();
    buscar_integrante();
}

function integrantes_directivas() {
    $('.Directiva').click(function () {
        $('#busqueda').val('');
        $('#Resultado').html("");
        var dire = "";
        var guardar = $('#contenedorDirectiva').html();
        var directiva = $(this).data('directiva');
        if (directiva === "Educacion") {
            dire += "de la directiva de Educación";
        }
        if (directiva === "TI") {
            dire += "del departamento de Tecnologias de la Información";
        }
        if (directiva === "RI") {
            dire += "de la directiva de Recursos Humanos";
        }
        if (directiva === "CH") {
            dire += "de la directiva de Capital Humano";
        }
        if (directiva === "EyL") {
            dire += "de la directiva de Educacion y Logistica";
        }
        if (directiva === "PMO") {
            dire += "de la Oficina de Gestion en Proyectos";
        }
        var data = {
            'directiva': directiva,
            'tag': 'V_directiva'
        };

        $.ajax({
            url: siteUrl + "personas/directiva",
            type: "post",
            data: data,
            success: function (msj) {
                var json = JSON.parse(msj);
                var html = "";
                html += "<h1>Miembros " + dire + "</h1>";
                html += "<div class='col-md-3'></div>";
                html += "<div class='col-md-5'><table class='table table-hover'>";
                html += "<th class='info'>Codigo</th><th class='info'>Nombres y Apellidos</th>";
                for (var i in json) {
                    html += "<tr data-toggle='modal' role='button' data-target='#integrante' ><td>" + json[i].id_usuarios + "</td><td>" + json[i].nombres + " " + json[i].apellidos + "</td></tr>";
                }
                html += "</table></div>";
                html += "<div class='col-md-4'></div>";
                html += "<div class='col-md-12'><div id='boton'><div class='btn-lg btn-primary' id='volviendo'>Retroceder</div></div></div>";
                html += introducir_html();
                $('#contenedorDirectiva').html(html);

                $('#personas table tr').click(function () {
                    $(this).addClass('select');
                    this.codigo = $('.select td').html();
                    $(this).removeClass('select');
                    console.log(this.codigo);
                    datos_de_integrante(this.codigo, json);
                });

                var guardarhover = "";

                $('#personas table tr').hover(function () {
                    $(guardarhover).removeClass("info");
                    guardarhover = $(this);
                    $(this).addClass("info");
                });

                $('#volviendo').click(function () {
                    $('#contenedorDirectiva').html(guardar);
                    $('.Directiva').on('click', integrantes_directivas);
                });



            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert('error');
            }
        });
    });

}
function buscar_integrante()
{

    $('#busqueda').hover(function () {
        if ($(this).val().length === 0) {
            $('#Resultado').html("");
        }
    });
    $('#Buscar').click(function ()
    {
        var html = "";
        if ($('#busqueda').val().length === 0) {
            $('#Resultado').html(html);
        }
        else {
            var envio = {
                'buscar': $('#busqueda').val()
            };
            $.ajax({
                url: siteUrl + 'personas/buscar_integrante',
                type: 'POST',
                data: envio,
                success: function (rpt) {
                    var json = JSON.parse(rpt);
                    html += "<h1>Miembros </h1>";
                    html += "<div class='col-md-3'></div>";
                    html += "<div class='col-md-5'><table class='table table-hover'>";
                    html += "<th>Codigo</th><th>Nombres y Apellidos</th>";
                    for (var i in json) {
                        html += "<tr data-toggle='modal' role='button' data-target='#integrante'><td>" + json[i].id_usuarios + "</td><td>" + json[i].nombres + " " + json[i].apellidos + "</td></tr>";
                    }

                    html += "</table></div>";
                    html += "<div class='col-md-4'></div>";

                    html += introducir_html();

                    $('#Resultado').html(html);
                    $('#personas table tr').click(function () {
                        $(this).addClass('select');
                        this.codigo = $('.select td').html();
                        $(this).removeClass('select');
                        console.log(this.codigo);
                        datos_de_integrante(this.codigo, json);

                    });


                }
            });
        }
    });
}
function introducir_html() {


    var html = "";
    html += "<div class='modal fade' id='integrante' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='display: none;'>";
    html += "<div class='modal-dialog'>";

    html += "           <div class='modal-content' id='contenido'>";
    html += "               <div id='imprimiendo'>\n\
                                <div id='marcaagua'><img src='"+baseUrl+"/assets/img/MarcaAgua.png'></div>";
    html += "			<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
    html += "                       <span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
    html += "			</button>";
    html += "			<div class='modal-header' align='center'>";
    html += "                       <div id='titulo'><h1>Acta de Proyectos Unac</h1></div>";
    html += "			</div>";
    html += "                   <div class='modal-body'>";
    html += "                       <div id='cintegrante'></div>";
    html += "                   </div>";
    html += "               <div class='modal-footer'>\n\
                                <div class='col-md-7'></div>\n\
                                <div class='col-md-5'>\n\
                                    <div id='Fpresidente'>\n\
                                            <div id='imagenFirma'><img src= '"+baseUrl+"assets/img/firmaPresi.png' id='firmaPresi'></div>\n\
                                            <div id='firma'>Presidente Proyectos Unac</div>\n\
                                    </div>\n\
                                </div>\n\
                                \n\
                            </div>\n\
                            <div class='col-md-12' id='imprimir'><div class='col-md-5'><div id='imprime'><span class='glyphicon glyphicon-print' aria-hidden='true'></span></div></div></div>";
    html += "           </div></div>";

    html += "</div>";
    return html;
}
function datos_de_integrante(codigo, json) {
    for (var i in json) {
        if (json[i].id_usuarios === codigo) {
            $('#personas #cintegrante').html("");
            if (json[i].proyectos !== null || json[i].cargo_proyecto !== null) {
                var stringP = json[i].proyectos;
                var proyectos = stringP.split(",");
                var stringS = json[i].cargo_proyecto;
                var cargo = stringS.split(",");
            }
            var Proyect = "";
            for (var ele in proyectos) {
                Proyect += "<tr>";
                for (var el in cargo) {
                    Proyect += "<td>" + proyectos[ele] + "</td>";
                    Proyect += "<td>" + cargo[el] + "</td>";
                }
                Proyect += "</tr>";
            }
            var html = "";

            html += "<div class='col-xs-12 col-sm-4 col-md-4 col-lg-4'><img src='"+baseUrl+"/assets/img/integrantes/" + json[i].foto + "' id='foto'></div>";

            html += "           <div class='col-md-12'>\n\
                                                <div class='col-md-1 col-lg-1'></div>\n\
                                                <div class='col-md-11 col-lg-11'><p id='dedicatoriaPU'>La Seccion estudiantil Proyectos Unac</p></div>\n\
                                                <div class='ool-md-12'><div id='tituloProyectos'><h3>Proyectos participados:</h3></div>\n\
                                                        <table class='table table-condensed'>\n\
                                                            <th>Proyecto</th><th>Cargo del Proyecto</th>\n\
                                                                " + Proyect + "</table>\n\
                                                </div>\n\
                                            </div>\n\
                                        ";
            $('#personas #cintegrante').append(html);
            
                $("#imprime").bind("click", function ()
                {
                     $("#contenido").printArea({
                        mode: "iframe",
                        standard: "html5",
                        popTitle: 'relatorio',
                        popClose: false,
                        extraCss: '../assets/css/estilos.css',
                        extraHead: "",
                        retainAttr: ["id", "class", "style"],
                        printDelay: 500, // tempo de atraso na impressao
                        printAlert: true,
                        printMsg: 'Aguarde a impressão'
                    });
                });
        }
    }
}