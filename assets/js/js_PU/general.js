var indicador = 0;
var pos = 0;
var intv;

$(document).on('ready', function () {

    init();
    intv = setInterval(handleClick, 8000);
    defineSizes();
    //login();
    login();
    intranet();
});
$(window).on('resize', defineSizes);
function defineSizes() {
    $('.form_container .slide').each(function (i, el) {
        $(el).css({
            'background-image': "url(" + $(el).data("background") + ")",
            'height': ($('.form_container').width() * 0.50) + 'px',
            'width': ($('.form_container').width()) + 'px'
        });
    });
    $('.form_container .slideContainer').css({
        'margin-left': -(indicador * $('.form_container').width()) + 'px'
    });
}

function init() {
    $('.slider_controls li').on('click', handleClick);

}
function handleClick() {
    var slide_target = 0;
    if ($(this).parent().hasClass('slider_controls')) {
        slide_target = $(this).index();
        pos = slide_target;
        clearInterval(intv);
        intv = setInterval(handleClick, 8000);
    }
    else {
        pos++;
        if (pos >= $('.slide').length) {
            pos = 0;
        }
        slide_target = pos;
    }
    $('.form_container .slideContainer').animate({
        'margin-left': -(slide_target * $('.form_container').width()) + 'px'
    }, 'slow');
}
function login() {

    if ($("#mensajeLogin").html() !== '') {
        $("#mensajeLogin").css({
            'border-radius': '10px 10px 10px 10px',
            '-moz-border-radius': '10px 10px 10px 10px',
            '-webkit-border-radius': '10px 10px 10px 10px',
            'border': '2px double  #e0102f',
            'padding': '1em'
        });
    }
    if ($("#Registrado").html() !== '') {
        $("#Registrado").css({
            'border-radius': '10px 10px 10px 10px',
            '-moz-border-radius': '10px 10px 10px 10px',
            '-webkit-border-radius': '10px 10px 10px 10px',
            'border': '2px double  #279153',
            'padding': '1em'
        });
    }

    $("#card").flip({
        trigger: 'manual'
    });
    $("#activado-btn").click(function (e) {
        e.preventDefault();
        $("#mensajeLogin").css("display", "none");
        $("#Registrado").css("display", "none");
        $("#card").flip(true);
    });
    $("#desactivado-btn").click(function (e) {
        e.preventDefault();
        $("#card").flip(false);
    });
    $("#automatico").click(function (e) {
        e.preventDefault();
        $("#redes").toggle();
    });
}
function intranet(){
    $("#apretar").click(function (e) {
        e.preventDefault();
        $("#SlideIntra").toggle();
       
    });
     $("#A-NavG").click(function (e) {
        e.preventDefault();
        $("#NavCel").toggle();
        
    });
     $("#NavUserT").click(function (e) {
        e.preventDefault();
        $("#Navuser").toggle();
    });
    $('input').focus(function(){
        $('.msj_error').css("display","none");
    });
}