$(document).on('ready', function () {

    iniciar();
});
google.maps.event.addDomListener(window, 'load', dibujarMapa);
function dibujarMapa() {
    var mapa;
    var opcionesMapa = {
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    mapa = new google.maps.Map(document.getElementById('googlemaps'), opcionesMapa);
    var geolocalizacion = new google.maps.LatLng(-12.062213003393458, -77.11820590000002);
    var marcador = new google.maps.Marker({
        map: mapa,
        draggable: false,
        position: geolocalizacion,
        visible: true
    });
    marcador.setTitle('Oficina de Proyectos Unac');

    mapa.setCenter(geolocalizacion);
    $('#llegar').on('click', function () {
        navigator.geolocation.getCurrentPosition(function (posicion) {
            calcRoute(geolocalizacion, mapa, posicion);
        });
    });
    $('#SituarOf').on('click', function () {
        dibujarMapa();
    });
}
function calcRoute(inicioRuta, mapa, posicion) {

    var DirectionsService = new google.maps.DirectionsService();
    var DirectionsRenderer = new google.maps.DirectionsRenderer();
    DirectionsRenderer.setMap(mapa);
    var posicionPU = new google.maps.LatLng(posicion.coords.latitude, posicion.coords.longitude);
    var marcador = new google.maps.Marker({
        map: mapa,
        draggable: false,
        position: posicionPU,
        visible: true
    });
    marcador.setTitle('Tu localización');
    var request = {
        origin: inicioRuta,
        destination: posicionPU,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    DirectionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            DirectionsRenderer.setDirections(response);
        }
    });
}
function iniciar() {
    $('#enviar').submit(function (e) {
        e.preventDefault();
        $error = false;
        if ($('#mensaje').val() === "") {
            $('#respuesta').html("Por Favor dejar su mensaje");
        }
        if ($('#asunto').val() === "") {
            $('#respuesta').html("Por Favor completar campo asunto");
        }

        if ($('#Email').val() === "") {
            $('#respuesta').html("Por Favor completar su email");
        }
        if ($('#Nombre').val() === "") {
            $('#respuesta').html("Por Favor completar el campo nombre");

        }

        if ($('#Nombre').val() !== "" && $('#Email').val() !== "" && $('#asunto').val() !== "" && $('#mensaje').val() !== "") {


            var data = {
                'nombre': $('#Nombre').val(),
                'correo': $('#email').val(),
                'asunto': $('#asunto').val(),
                'mensaje': $('#mensaje').val(),
                'tag': 'gmail'
            };
            $.ajax({
                url: siteUrl + "contacto/enviar",
                type: "post",
                data: data,
                success: function (msj) {
                    if (msj === "Enviado") {

                        $('#respuesta').html("Su mensaje ha sido enviado satisfactioriamente. Gracias");
                        $('#enviar').each(function () {
                            this.reset();
                        });
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('error');
                }
            });
        }
    });
}