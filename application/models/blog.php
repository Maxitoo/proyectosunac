<?php

class Blog extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    public function lista_entrada(){
        $this->db->select('*')
                ->order_by('fecha','desc');
        $consulta=$this->db->get('entradas');
        return $consulta->result();
    }
    public function articulo($permiso_url){
        $this->db->select('*')
                ->where('permalink',$permiso_url);
        $consulta=$this->db->get('entradas');
        return $consulta->row();
    }
}

