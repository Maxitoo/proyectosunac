<?php

class Verificar_usuarios extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function login($email, $pass) {
        $this->db->select('correo')
                ->where('correo', $email);
        $consulta = $this->db->get('usuarios');
        if ($consulta->num_rows() > 0) {
            $this->db->select('*')
                    ->where('correo', $email)
                    ->where('password', $pass)
                    ->from('usuarios');
            $consulta = $this->db->get();
            if ($consulta->num_rows() > 0) {
                $respuesta = $consulta->row();
                $nuevosdatos = array(
                    'id_user' => $respuesta->id_usuarios
                );

                $this->session->set_userdata($nuevosdatos);
                redirect(base_url() . "panel");
            } else {
                $mensaje = '<h2>Error al ingresar los datos</h2>'
                        . '<p>El password ingresado es incorrecto'
                        . ' por favor intentelo denuevo</p>';
                $this->session->set_flashdata('CorreoLogin', $mensaje);
                redirect(base_url() . "intranet/login");
            }
        } else {
            $mensaje = '<h2>Error al ingresar los datos</h2>'
                    . '<p>La dirección de correo proporcionada es incorrecta'
                    . ' por favor intentelo denuevo</p>';
            $this->session->set_flashdata('CorreoLogin', $mensaje);
            redirect(base_url() . "intranet/login");
        }
    }
    function verificarFacebook($data){
        if($this->check_email($data['email'])){
            $red_social=$this->db->select('id_usuarios,red_social,correo')
                        ->from('usuarios')
                        ->where('correo',$data['email'])
                        ->get()->row_array();
            if($red_social['red_social']!=null && $red_social['red_social']=="facebook"){

                $nuevosdatos = array(
                    'id_user' => $red_social['id_usuarios']
                );
                $this->session->set_userdata($nuevosdatos);
                redirect(base_url()."panel");
            }
            else{
                redirect("intranet/login");
            }
        }
        else{
            $data = array(
                'nombres' => $data['first_name'] ,
                'apellidos' => $data['last_name'] ,
                'genero' => $data['gender'],
                'correo' => $data['email'],
                'tipo_usuario' => 'externo',
                'anio_ingreso' => date("Y-m-d H:i:s"),
                'red_social'=>'facebook',
                'foto'=>'https://graph.facebook.com/'.$data['id'].'/picture?type=large'
            );
            $this->db->insert('usuarios',$data);
            $id=$this->db->insert_id();
            $user_externo = array(
                'id_usuarios'=>$id
            );
            $this->db->insert('user_externo',$user_externo);
            if($id>0){
                $nuevosdatos = array(
                    'id_user' => $id
                );
                $this->session->set_userdata($nuevosdatos);
                redirect(base_url()."panel");
            }
            else{
                redirect("intranet/login");
            }
        }
    }
    function check_email($correo){
        $this->db->select('correo')->where('correo',$correo);
        $consulta=$this->db->get('usuarios');
        if($consulta->num_rows()>0){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    function guardar($nombre, $apellido, $correo, $password) {

        $this->db->select('correo')
                ->where('correo', $correo);
        $consulta = $this->db->get('usuarios');
        if ($consulta->num_rows() > 0) {
            
            $mensaje = '<h2>Error al registrar los datos</h2>'
                    . '<p>La dirección de correo proporcionada ya esta en uso'
                    . '</p>';
            $this->session->set_flashdata('CorreoLogin', $mensaje);
            redirect(base_url() . "intranet/registro");
        } 
        else {
            $data = array(
                'nombres' => $nombre,
                'apellidos' => $apellido,
                'correo' => $correo,
                'password'=>$password,
                'tipo_usuario' => 'externo'
            );
            
            $this->db->insert('usuarios', $data);
            $id=$this->db->insert_id();
            $data = array(
                'id_usuarios'=>$id
            );
            $this->db->insert('user_externo', $data);
            
            $mensaje = '<h2>Exito del registro</h2>'
                    . '<p>Usted fue registrado exitosamente. Gracias por confiar en nosotros.'
                    . '</p>';
            $this->session->set_flashdata('Registrado', $mensaje);
            redirect(base_url() . "intranet/login");
        }
    }

    function info_user($ID) {
        $this->db->select('*')
                ->where('id_usuarios', $ID);
        $consulta = $this->db->get('usuarios');
        return $respuesta = $consulta->row();

    }

}
