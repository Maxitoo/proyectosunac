<?php

class Chat extends CI_Model{
    public function __construct() {
        parent::__construct();
    }
    function ver_mensajes(){
        $this->db->select('*');
        $consulta=$this->db->get('chat');
        return $consulta->result();
        /*$this->db->where('TIMESTAMPDIFF(SECOND,fecha_creado,NOW()) <= 2',null,false);
        $this->db->where('m_chat_usuario !=',$notnick);
        $query = $this->db->get('chat');
        return $query->result_array();*/
    }
    function agregar_mensaje($usuario,$mensaje){
        $resultado=$this->db->select('*')
                
                ->order_by('id_chat_mensaje','desc')
                ->get('chat',1)->row();
        if($resultado->m_chat_usuario==$usuario){
             $this->db->where('id_chat_mensaje',$resultado->id_chat_mensaje);
             $this->db->set('m_chat_usuario',$usuario);
             $this->db->set('chat_mensaje',$resultado->chat_mensaje."<br/>".$mensaje);
            $this->db->set('fecha_creado', 'NOW()', FALSE);
            return $this->db->update('chat');
        }
        else{
            $data = array(
               'm_chat_usuario' => $usuario ,
               'chat_mensaje' => $mensaje
            );
            $this->db->set('fecha_creado', 'NOW()', FALSE);
            return $this->db->insert('chat', $data);
        }
        
    }
}
