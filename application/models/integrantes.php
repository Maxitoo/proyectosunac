<?php

class Integrantes extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function user_directivas($tabla_directiva) {

        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->join('user_miembro', 'usuarios.id_usuarios = user_miembro.id_usuarios', 'INNER');
        $this->db->join('user_pertenece', 'usuarios.id_usuarios = user_pertenece.id_usuarios', 'INNER');
        $this->db->join($tabla_directiva, $tabla_directiva .'.id_usuarios = user_pertenece.id_usuarios', 'INNER');
        $respuesta = $this->db->get();
        return $respuesta->result();
    }

    public function busqueda_integrantes($buscar) {
        $this->db->select('*');
        $this->db->like('nombres', $buscar);
        $this->db->or_like('apellidos', $buscar); 
        $this->db->join('user_miembro', 'usuarios.id_usuarios = user_miembro.id_usuarios', 'INNER');

        $respuestaA = $this->db->get('usuarios',5);
        $objetoA = $respuestaA->result();

        return $objetoA;
    }

}
