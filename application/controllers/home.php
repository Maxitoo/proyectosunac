<?php
class Home extends PU_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('verificar_usuarios','loginModel');
        $this->load->library('facebook');
    }
    //Pagina Principal
    function index(){
        $datos['css']=array('Ehome.css');
        $datos['titulo']='Proyectos Unac';
        $datos['view']='home/index';
        $datos['slider']='ver';
        $datos['fb_coment']=FALSE;
        $this->cargarVista($datos);
    }
    public function facebookRequest(){
        $permisos=['public_profile','email'];
        redirect($this->facebook->urlLogin(base_url(),$permisos));
    }
    public function facebookCallback()
    {
        $user = $this->facebook->callback();
        if (is_string($user)) {
            echo $user;
        }
        if (!empty($this->session->userdata('facebook_access_token')) && !is_string($user)) {
            $data = array(
                "email" => $user['email'],
                "name" => $user['name'],
                "first_name" => $user['first_name'],
                "last_name" => $user['last_name'],
                "gender" => $user['gender'],
                "link" => $user['link'],
                "locale" => $user['locale'],
                "id"=>$user['id']
            );
            $this->loginModel->verificarFacebook($data);

        }
    }
}