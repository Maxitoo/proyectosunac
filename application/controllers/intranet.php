<?php

class Intranet extends PU_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    function login() {
        $datos['css']=array('Eintranet.css');
        $datos['scripts']=array('Eintranet.js');
        $datos['titulo'] = 'Sobre Proyectos Unac';
        $datos['view'] = 'intranet/login_view';
        $datos['slider'] = 'eliminar';
        $datos['fb_coment']=FALSE;
        $this->cargarVista($datos);
    }

    function check_email($correo) {
        $this->load->database();
        $this->load->model('verificar_usuarios');
        if ($this->verificar_usuarios->check_email($correo)) {
            $this->form_validation->set_message('check_email', 'El correo ya esta en uso');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function registro() {

        $this->form_validation->set_message('required', 'Campo Obligatorio');
        $this->form_validation->set_message('min_length', 'Min 5 caracteres');
        $this->form_validation->set_error_delimiters('<div class="msj_error">', '</div>');
        if ($this->form_validation->run() === FALSE) {
            $datos['css']=array('Eintranet.css');
            $datos['titulo'] = 'Sobre Proyectos Unac';
            $datos['view'] = 'intranet/registro_view';
            $datos['slider'] = 'eliminar';
            $datos['fb_coment'] = FALSE;
            $this->cargarVista($datos);
        } else {

            $this->load->database();
            $this->load->model('verificar_usuarios');
            $nombres = $this->security->xss_clean(strip_tags($this->input->post('nombres')));
            $apellidos = $this->security->xss_clean(strip_tags($this->input->post('apellidos')));
            $correo = $this->security->xss_clean(strip_tags($this->input->post('email')));
            $password = md5($this->security->xss_clean(strip_tags($this->input->post('password'))));
            $this->verificar_usuarios->guardar($nombres, $apellidos, $correo, $password);

            redirect(base_url() . "intranet/registro");
        }
    }

    function identificar() {
        $datos['css']=array('Eintranet.css');
        $datos['scripts']=array('Eintranet.js');
        $datos['titulo'] = 'Sobre Proyectos Unac';
        $datos['view'] = 'intranet/identificar_view';
        $datos['slider'] = 'eliminar';
        $datos['fb_coment'] = FALSE;
        $this->cargarVista($datos);
    }

    function usuario() {
        if (
            $this->session->userdata('id_user') !== FALSE) {
            $this->load->database();
            $this->load->model('verificar_usuarios');
            $this->load->model('blog');
            $info_user = $this->verificar_usuarios->info_user($this->session->userdata('id_user'));
            $list_entrada = $this->blog->lista_entrada();
            $datos['css']=array('Pusuario.css');
            $datos['titulo'] = 'Sobre Proyectos Unac';
            $datos['view'] = 'intranet/usuario_view';
            $datos['slider'] = 'eliminar';
            $datos['info_user'] = $info_user;
            $datos['lista_entradas'] = $list_entrada;
            $datos['fb_coment']=FALSE;
            $this->cargarVista($datos);
        } else {
            redirect(base_url());
        }
    }


    function verificar() {
        $this->load->database();
        $this->load->model('verificar_usuarios');
        $correo = $this->security->xss_clean(strip_tags($this->input->post('email')));
        $password = md5($this->security->xss_clean(strip_tags($this->input->post('password'))));
        $this->verificar_usuarios->login($correo, $password);
    }

    function salir() {
        $borrarDatos = array(
            'id_user' =>''
        );
        $this->session->unset_userdata($borrarDatos);
        $this->session->sess_destroy();
        redirect(base_url());
    }

}
