<?php

class Contacto extends PU_Controller {

    public function __construct() {
        parent::__construct();
    }

    //Pagina Principal
    function index() {

        $datos['css']=array('Econtacto.css');
        $datos['scripts']=array('Econtacto.js');
        $datos['pluginsExternos']=array('http://maps.googleapis.com/maps/api/js?sensor=true');
        $datos['titulo'] = 'Contacta a Proyectos Unac';
        $datos['view'] = 'contacto/contacto_view';
        $datos['slider']='eliminar';
        $datos['fb_coment']=FALSE;
        $this->cargarVista($datos);
    }

    function enviar() {
        if (!$this->input->is_ajax_request()) {
            redirect('404');
        } else {
            $this->load->library('email');
            $nombre = $this->input->post('nombre');
            $destinatario = 'proyectos_unac@pmi.org.pe';
            $asunto = $this->input->post('asunto');
            $mensaje = $this->input->post('mensaje');
            $correo =$this->input->post('correo');
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $enviar_mensaje=generar_html($nombre,$correo,$mensaje,base_url());
            $this->email->from($correo, $nombre);
            $this->email->to($destinatario);
            $this->email->subject($asunto);
            $this->email->message($enviar_mensaje);
            $this->email->send();

            echo "Enviado";
        }
        exit;
    }


}

    function generar_html($nombre,$correo,$mensaje,$base_url) {

        $html = '<table width = "828" height = "248" cellspacing = "0" cellpadding = "0" border = "0">
        <tbody>
        <tr>
        <td valign = "top" bgcolor = "#77B2FF"><font color = "#ffffff"><blockquote><img width = "107" height = "91" src = "'.$base_url.'/assets/img/Logo.png" alt = "Proyectos Unac" class = "CToWUd"></blockquote></font></td>
        <td valign = "top" bgcolor = "#77B2FF"><font color = "#ffffff"><br>
        </font></td>
        <td valign = "top" bgcolor = "#77B2FF"><font color = "#ffffff"><blockquote><img width = "107" height = "91" src = "'.$base_url.'/assets/img/Logo.png" alt = "Proyectos Unac" class = "CToWUd"></blockquote></font><br>
        <font color = "#ffffff">
        </font></td>
        </tr>
        <tr>
        <td valign = "top" bgcolor = "#77B2FF"><font color = "#ffffff"><br>
        </font></td>
        <td valign = "top" bgcolor = "#77B2FF" align = "center">
        <h3><font color = "#ffffff"><font face = "AR JULIAN"><big>&nbsp;
        <u>Este
        </u><u>mensaje fue enviado de</u><u>s</u><u>de la
        pagina web de Proyectos Unac</u></big></font><br>
        </font></h3>
        </td>
        <td valign = "top" bgcolor = "#77B2FF"><br>
        </td>
        </tr>
        <tr>
        <td valign = "top" bgcolor = "#ffffff"><font color = "#ffffff"><br>
        </font></td>
        <td valign = "top" bgcolor = "#ffffff"><font color = "#ffffff"><br>
        </font></td>
        <td valign = "top" bgcolor = "#ffffff"><font color = "#ffffff"><br>
        </font></td>
        </tr>
        <tr>
        <td valign = "top" bgcolor = "#ffffff"><font color = "#ffffff"><br>
        </font></td>
        <td valign = "top" bgcolor = "#ffffff">
        <blockquote>Enviado por : '.$nombre.'.<br>
        </blockquote>
        </td>
        <td valign = "top" bgcolor = "#ffffff"><font color = "#ffffff"><br>
        </font></td>
        </tr>
        <tr>
        <td valign = "top"><br>
        </td>
        <td valign = "top">
        <blockquote>Correo : '.$correo.'<br>
        </blockquote>
        </td>
        <td valign = "top"><br>
        </td>
        </tr>
        <tr>
        <td valign = "top"><br>
        </td>
        <td valign = "top">
        <blockquote>Mensaje : '.$mensaje.'</blockquote>
        </td>
        <td valign = "top"><br>
        </td>
        </tr>
        <tr>
        <td valign = "top" bgcolor = "#77B2FF"><br>
        </td>
        <td valign = "top" bgcolor = "#77B2FF" align = "center"><font color = "#ffffff">&nbsp;
        Por favor responder lo antes posible.<br>
        </font></td>
        <td valign = "top" bgcolor = "#77B2FF"><font color = "#ffffff"><br>
        </font></td>
        </tr>
        </tbody>
        </table>';
        return $html;
    }