<?php

class Eventos extends PU_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $this->load->database();
        $this->load->model('blog');
        $list_entrada = $this->blog->lista_entrada();
        $datos['css']=array('Eeventos.css');
        $datos['titulo'] = 'Sobre Proyectos Unac';
        $datos['view'] = 'eventos/eventos_view';
        $datos['slider'] = 'eliminar';
        $datos['lista_entradas'] = $list_entrada;
        $datos['fb_coment'] = FALSE;
        $this->cargarvista($datos);
    }

    function detalle($permiso_url) {
            $this->load->database();
            $this->load->model('verificar_usuarios');
            $this->load->model('blog');
           
            $list_entrada = $this->blog->articulo($permiso_url);
            $datos['css']=array('Eeventos.css');
            $datos['titulo'] = 'Sobre Proyectos Unac';
            $datos['view'] = 'eventos/articulo_view';
            $datos['slider'] = 'eliminar';
            $datos['lista_entradas'] = $list_entrada;
            $datos['fb_coment'] = TRUE;
            $this->cargarVista($datos);

    }

    
}
