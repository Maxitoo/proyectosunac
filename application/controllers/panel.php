<?php
/**
 * User: MaxJordan
 * Date: 06/03/2016
 * Time: 12:48 PM
 */
class Panel extends PU_Controller{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        if ($this->session->userdata('id_user') !== FALSE) {
            $this->load->model('verificar_usuarios');
            $this->load->model('blog');
            $info_user = $this->verificar_usuarios->info_user($this->session->userdata('id_user'));
            $list_entrada = $this->blog->lista_entrada();
            $datos['css']=array('Pusuario.css');
            $datos['titulo'] = 'Panel Proyectos Unac';
            $datos['view'] = 'panel/usuario_view';
            $datos['slider'] = 'eliminar';
            $datos['info_user'] = $info_user;
            $datos['lista_entradas'] = $list_entrada;
            $datos['fb_coment']=FALSE;
            $this->cargarVista($datos);
        } else {
            redirect(base_url());
        }
    }
}