<?php 

class Personas extends PU_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('integrantes');
    }

    //Pagina Principal
    public function index() {
        $datos['css']=array('Eintegrantes.css','PrintArea.css');
        $datos['scripts']=array('Eintegrantes.js');
        $datos['plugins']=array('jquery.PrintArea.js');
        $datos['titulo'] = 'Integrantes en Proyectos Unac';
        $datos['view'] = 'personas/personas_view';
        $datos['slider']='eliminar';
        $datos['fb_coment']=FALSE;
        $this->cargarVista($datos);
    }

    public function directiva()
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else
        {
            
            $this->load->database();
            if($this->input->post('directiva')=="Educacion"){
                $info=$this->integrantes->user_directivas("d_e");
                echo json_encode($info);
                
            }
            if($this->input->post('directiva')=="TI"){
                $info=$this->integrantes->user_directivas("d_ti");
                echo json_encode($info);
            }
            if($this->input->post('directiva')=="RI"){
                $info=$this->integrantes->user_directivas("d_ri");
                echo json_encode($info);
            }
            if($this->input->post('directiva')=="CH"){
                $info=$this->integrantes->user_directivas("d_ch");
                echo json_encode($info);
            }
            if($this->input->post('directiva')=="EyL"){
                $info=$this->integrantes->user_directivas("d_eyl");
                echo json_encode($info);
            }
            if($this->input->post('directiva') == "PMO"){
                $info=$this->integrantes->user_directivas("pmo");
                echo json_encode($info);
            }
            exit;
        }
    }
    public function buscar_integrante()
    {
        if(!$this->input->is_ajax_request())
        {
            redirect('404');
        }
        else
        {
            $this->load->database();
            $info=$this->integrantes->busqueda_integrantes($this->input->post('buscar'));
            echo json_encode($info);
        }
        exit;
    }

}
