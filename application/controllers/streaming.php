<?php

class Streaming extends PU_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        if($this->session->userdata('streaming_user')===FALSE) {
            $datos['css']=array('Estreaming.css');
            $datos['titulo'] = 'Streaming Proyectos Unac';
            $datos['view'] = 'streaming-user';
            $datos['slider'] = 'eliminar';
            $datos['config'] = 'streaming';
            $datos['fb_coment'] = 'FALSE';
            $this->cargarVista($datos);
        } else {
            
            $this->load->database();
            $this->load->model('chat');
            $datos['scripts']=array('Estreaming.js');
            $datos['css']=array('Estreaming.css');
            $datos['mensajes'] = $this->chat->ver_mensajes();
            $datos['titulo'] = 'Streaming Proyectos Unac';
            $datos['view'] = 'streaming';
            $datos['slider'] = 'eliminar';
            $datos['fb_coment'] = 'FALSE';
            $this->cargarVista($datos);
        }
    }
    function admin(){
        
    }
    function agregar_mensaje(){
        $mensaje = $this->input->post('mensaje');
        $usuario =$this->session->userdata('streaming_user');
        $this->load->database();
        $this->load->model('chat');
        $this->chat->agregar_mensaje($usuario, $mensaje);
    }

    function ver_mensajes() {
        $this->load->database();
        $this->load->model('chat');
        $mensajes = $this->chat->ver_mensajes();
        echo json_encode($mensajes);
        exit;
    }
    function agregar_usuario(){
        $nombre_user=$this->input->post('nombre_user');
        $this->session->set_userdata('streaming_user',$nombre_user);
        redirect(base_url()."streaming");
    }

}
