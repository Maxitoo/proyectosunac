<?php

class Nosotros extends PU_Controller{
    public function __construct() {
        parent::__construct();
    }
    function index(){
        $datos['css']=array('Enosotros.css');
        $datos['titulo']='Sobre Proyectos Unac';
        $datos['view']='nosotros/nosotros_view';
        $datos['slider']='eliminar';
        $datos['fb_coment']=FALSE;
        $this->cargarVista($datos);
    }
}

