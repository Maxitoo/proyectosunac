<?php
 $config = array(
            array(
                'field' => 'nombres',
                'label' => 'Nombre',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'apellidos',
                'label' => 'Apellido',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Correo',
                'rules' => 'trim|required|callback_check_email'
            ),
            
            array(
                'field' => 'password',
                'label' => 'Contraseña',
                'rules' => 'trim|required|min_length[5]'
            )
        );