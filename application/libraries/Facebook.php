<?php
/**
 * User: MaxJordan
 * Date: 05/03/2016
 * Time: 11:43 PM
 */
require_once('Facebook/autoload.php');
class Facebook {
    protected $CI;
    private $facebook;
    private $helper;
    public function __construct()
    {
        session_start();
        $this->CI=& get_instance();
        $this->CI->load->library('session');
        $this->CI->config->load('facebook');
        $this->facebook = new Facebook\Facebook($this->CI->config->config);
        return $this->facebook;
    }
    public function urlLogin($baseUrl,$permisos){
        $fb=$this->facebook;
        $this->helper=$fb->getRedirectLoginHelper();
        $loginUrl = $this->helper->getLoginUrl($baseUrl.'home/facebookCallback', $permisos);
        return $loginUrl;
    }
    public function callback(){
        try
        {   $this->helper=$this->facebook->getRedirectLoginHelper();
            $accessToken = $this->helper->getAccessToken();

            $this->CI->session->set_userdata('facebook_access_token', (string) $accessToken);
            $this->facebook->setDefaultAccessToken($this->CI->session->userdata('facebook_access_token'));

            $response = $this->facebook->get('me?fields=id,name,email,age_range,gender,first_name,last_name,is_shared_login,is_verified,link,picture{url},locale');
            $userNode = $response->getGraphUser();
            //if Logged return array data user
            return $userNode;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            return 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            return 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }
    public function infoUser($baseUrl,$code){


        /*if($_REQUEST['state'] == $_SESSION['state']) {
            $token_url = "https://graph.facebook.com/oauth/access_token?"
                . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
                . "&client_secret=" . $app_secret . "&code=" . $code;

            $response = @file_get_contents($token_url);
            $params = null;
            parse_str($response, $params);

            $graph_url = "https://graph.facebook.com/me?access_token="
                . $params['access_token'];

            $user = json_decode(file_get_contents($graph_url));
            echo("Hello " . $user->name);
        }
        else {
            echo("The state does not match. You may be a victim of CSRF.");
        }*/

    }


}