
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $titulo ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/PrintArea.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/estilos.css" media="all">

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <?php
            if(isset($css)){
                foreach ($css as $agregar_css) { ?>
                    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/<?php echo $agregar_css; ?>" media="all">
                <?php }
            }
        ?>
        <script src="<?php echo base_url(); ?>assets/js/vendor/modernizr-2.6.2.min.js"></script>

    </head>
    <?php if ($fb_coment !== FALSE) { ?>
        <div id = "fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.5";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
    <?php } ?>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <header>

            <nav id="NavP">
                <a class="navbar-brand" href="#">
                    <img id="LogoNav" alt="Brand" src="<?php echo base_url(); ?>assets/img/Logo.png"/>
                </a>
                <ul class="hidden-xs" id="navegando">

                    <a href="<?php echo base_url(); ?>"><li>Inicio</li></a>
                    <a href="<?php echo base_url(); ?>nosotros"><li>Nosotros</li></a>
                    <a href="<?php echo base_url(); ?>personas"><li>Personas</li></a>
                    <a href="<?php echo base_url(); ?>eventos"><li>Eventos</li></a>
                    <a href="<?php echo base_url(); ?>contacto"><li>Contacto</li></a>
                </ul>
                <ul class="visible-xs" id="NavCell">
                    <a href="#"><li id="A-NavG"><span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span></li></a>
                    <ul id="NavCel">

                        <a href="<?php echo base_url(); ?>"><li >Inicio</li></a>
                        <a href="<?php echo base_url(); ?>nosotros"><li>Nosotros</li></a>
                        <a href="<?php echo base_url(); ?>personas"><li>Personas</li></a>
                        <a href="<?php echo base_url(); ?>eventos"><li>Eventos</li></a>
                        <a href="<?php echo base_url(); ?>contacto"><li>Contacto</li></a>
                        <?php if ($slider !== 'eliminar' && FALSE === $this->session->userdata('id_user')) { ?><a href="<?php echo base_url(); ?>intranet/login"><li class="visible-xs" >Acceder</li></a><?php } ?>

                        <?php if ($this->session->userdata('id_user') !== FALSE) { ?><a href="<?php echo base_url(); ?>panel"><li class="visible-xs" >Mi Perfil</li></a><?php } ?>
                    </ul>
                </ul>
                <?php if ( $this->session->userdata('id_user')===FALSE) { ?>
                    <ul class="hidden-xs" id="intranetNav">
                        <a href="#" id="apretar" ><li >Acceder <span class="caret" aria-hidden="true"></span></li></a>
                        <div id="SlideIntra">
                            <li>
                                <div class="row" id="intranet">
                                    <div class="col-md-12" >
                                        Ingresa por: 
                                        <div class="social-buttons" >
                                            <a href="<?php echo base_url();?>home/facebookRequest" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                            <a href="#" class="btn btn-tw"><i class="fa fa-google"></i> Google</a>
                                        </div>
                                        o por cuenta propia:
                                        <form class="form" role="form" method="post" action="<?php base_url(); ?>intranet/verificar" accept-charset="UTF-8" id="login-nav">
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                <input type="email" name="email" class="form-control" id="email" placeholder="Email address" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                                <div class="help-block text-right"><a href="<?php base_url(); ?>intranet/identificar">¿Olvidaste tu contraseña?</a></div>
                                            </div>
                                            <div id="mensajeLogin"><?php echo $this->session->flashdata('CorreoLogin', 300); ?></div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
                                            </div>
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> No cerrar sesión
                                                </label>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="bottom text-center">
                                        ¿No tienes cuenta? <a  href="<?php base_url(); ?>intranet/registro"><b>Unetenos</b></a>
                                    </div>  
                                </div>
                            </li>
                        </div>
                    </ul><?php } ?>
                <?php if ($this->session->userdata('id_user') !== FALSE) { ?>
                    <ul id="intranetNav" class=" hidden-xs">
                        <a href="#" id="NavUserT"><li  ><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <span class="caret"></span></li></a>

                        <ul id="Navuser">
                            <a href="<?php echo base_url(); ?>panel"><li><span class="glyphicon glyphicon-user" aria-hidden="true"></span>&nbsp;Mi Perfil</li></a>
                            <a href="<?php echo base_url(); ?>intranet/salir"><li><span class="glyphicon glyphicon-off" aria-hidden="true"></span>&nbsp;Salir</li></a>
                        </ul>

                    </ul>
                <?php }; ?>
            </nav>
            <?php if ($slider !== 'eliminar') { ?>
                <div id="sliderheader">

                    <div class='form_container '>
                        <div class="slideContainer fullScreen">

                            <div class="slide fullScreen" data-background='<?php echo base_url(); ?>assets/img/torta.jpg'>

                            </div>


                            <div class="slide fullScreen" data-background='<?php echo base_url(); ?>assets/img/equipo_proyectos_1.jpg'>

                            </div>


                            <div class="slide fullScreen" data-background='<?php echo base_url(); ?>assets/img/equipo_proyectos_2.jpg'>

                            </div>


                            <div class="slide fullScreen" data-background='<?php echo base_url(); ?>assets/img/equipo_proyectos_3.jpg'>

                            </div>

                        </div>
                        <div class="slider_controls">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </div>
                    </div>
                </div>
            <?php }; ?>
        </header>
        <script >
            var baseUrl="<?php echo base_url(); ?>" ;
            var siteUrl="<?php echo site_url(); ?>" ;

        </script>