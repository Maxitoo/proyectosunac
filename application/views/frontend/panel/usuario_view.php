<section>
    <div id="Header-perfil">
        <div class="container" id="infouser">
            <div class="col-xs-0 col-sm-0 col-md-1 col-lg-1"></div>
            <div class="col-xs-12 col-sm-4 col-md-3 col-lg-3"><img id="Foto-user" src="<?php echo base_url();?>assets/img/integrantes/max.jpg"></div>
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8"><p id="Nombres"><?php
                    echo $info_user->nombres . " ";
                    echo $info_user->apellidos;
                    ?></p>

                <li id="dato"><p>E-mail:</p> <?php echo $info_user->correo; ?></li>
                <li id="dato"><p>Telefono:</p> <?php
                    if ($info_user->telefono === FALSE) {
                        echo $info_user->telefono;
                    } else {
                        echo "Completar Campo";
                    }
                    ?></li>
                <li id="dato"><button class="btn btn-danger">Editar Perfil</button></li>
            </div>
        </div>
        <div class="hidden-xs" id="LogoP"></div>
    </div>
    <div  id="adorno"></div>

</section>
<section>
    <div id="blog">
        <div class="container">
            <?php
            foreach ($lista_entradas as $campo):
                $url = 'evento/' . $campo->permalink;
                ?>
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="<?php echo base_url() . "assets/img/eventos/" . $campo->imagen; ?>" alt="...">
                        <div class="caption">
                            <h3><?php echo $campo->titulo; ?></h3>
                            <?php echo $campo->fecha; ?>
                            <p><?php
                                echo $campo->tags."... ";
                                echo anchor($url, "leer mas");
                                ?></p>
                            <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                        </div>
                    </div>
                </div>
<?php endforeach; ?>
        </div>
    </div>
</section>
