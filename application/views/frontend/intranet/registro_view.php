<div id="logeo">
    <div >
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6">
            <div id="card"> 
                <div id="formulariologeo" class="front"> 
                    <div id="mensajeLogin"><?php echo $this->session->flashdata('CorreoLogin'); ?></div>
                    <div id="Registrado"><?php echo $this->session->flashdata('Registrado'); ?></div>
                    <div  id="login">
                        <div>
                            <h1>Registrate</h1>
                            <form class="form" name="form" role="form" method="post" action="<?php base_url(); ?>registro" accept-charset="UTF-8" id="login-nav">
                                <div class="form-group">
                                    <label for="nombre">Nombres:</label>
                                    <input type="text" name="nombres" class="form-control"  id="nombre" placeholder="Nombres" value="<?php echo set_value('nombres');?>">
                                    <?php echo form_error('nombres'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="apellidos">Apellidos:</label>
                                    <input type="text" name="apellidos" class="form-control"  id="apellidos" placeholder="Apellidos"value="<?php echo set_value('apellidos');?>">
                                    <?php echo form_error('apellidos'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="correo">E-mail:</label>
                                    <input type="email" name="email" class="form-control" id="correo" value="<?php echo set_value('email');?> " placeholder="Email address">
                                    <?php echo form_error('email'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="pass">Password:</label>
                                    <input type="password" name="password" class="form-control" id="pass" placeholder="password">
                                    <?php echo form_error('password'); ?>
                                </div>
                                <div id="redes">
                                    <h3>Registro automatico</h3>
                                    <div class="social-buttons"id="redesSociales" >
                                        <a href="<?php echo base_url();?>home/facebookRequest" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                        <a href="#" class="btn btn-tw"><i class="fa fa-google"></i> Google</a>
                                    </div>
                                    <p>Recuerda que necesitamos enviar información al correo que mas uses.</p>
                                </div>
                                <div id="botones">

                                    <button class="btn btn-primary" id="automatico">Automatico</button>
                                    <input class="btn btn-primary" type="submit" value="Registro"/> 
                                    <button id="activado-btn" class="btn btn-primary">Logeate</button>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
                <div id="formulariologeo" class="back">


                    
                    <div  id="login">
                        <div id="logeando">
                            <h1>Inicia Sesión</h1>
                            <p>Ingresa por:</p> 
                            <div class="social-buttons " id="redesSociales">
                                <a href="<?php echo base_url();?>home/facebookRequest" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                <a href="#" class="btn btn-tw"><i class="fa fa-google"></i> Google</a>
                            </div>
                            <p>o por cuenta propia:</p>
                            <form class="form" role="form" method="post" action="<?php base_url(); ?>verificar" accept-charset="UTF-8" id="login-nav">
                                <div class="form-group">
                                    <label class="sr-only" for="email">Email address:</label>
                                    <input type="email" name="email" class="form-control" id="email" placeholder="Email address" required>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="password">Password:</label>
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                    <div class="help-block text-right"><a href="<?php base_url(); ?>identificar">¿Olvidaste tu contraseña?</a></div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
                                </div>
                                <div >
                                    <label>
                                        <input type="checkbox"> No cerrar sesión
                                    </label>
                                </div>
                            </form>
                        </div>

                    </div>

                    <div class="bottom text-center">
                        ¿No tienes cuenta? <div id="desactivado-btn"><b>Unetenos</b></div>
                    </div>
                </div> 
            </div>
        </div>
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
    </div>
</div>
