<div id="recuperar">
    <div class="container">
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6">
            <div id="formularioRecu">
                <h1>Recupera tu cuenta</h1>
                <p>Por favor introduce tu correo electronico, para poder restablecer los datos de tu cuenta.</p>
                <form class="form-inline" role="form" method="post" action="<?php base_url(); ?>verificar" accept-charset="UTF-8" id="login-nav">
                    <div class="form-group">
                        <label class="sr-only" for="recuperar">Password:</label>
                        <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></div>
                            <input type="email" class="form-control" id="recuperando" placeholder="E-mail" required>
                        </div>

                    </div>
                    <div class="form-group" id="Erecuperando">
                        <input type="submit" class="btn btn-primary" value="Enviar">
                        <a href="<?php base_url()?>login" type="button" class="btn btn-primary">Cancelar</a>
                    </div>
                </form>    
            </div>
        </div>
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
    </div>
</div>