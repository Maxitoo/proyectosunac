<section id="Info">

    <div class="container">
        <div id="TituloGeneral"><h1>Conoce mas de Proyectos Unac</h1></div>
        <div class="col-md-4"><div class="circulo" id="Nosot">
                <img class="fotoNoso" src="<?php base_url(); ?>assets/img/Logo.png"></div>
            <div class="TituloInicio"><h1>Nosotros</h1></div>
            <div class="DescripcionIni"><p>Proyectos Unac con mas de 10 años </p></div>

        </div>
        <div class="col-md-4"><div class="circulo" id="Integ">
                <img class="fotoNoso" src="<?php base_url(); ?>assets/img/Nosotros.png"></div>
            <div class="TituloInicio"><h1>Integrantes</h1></div>
            <div class="DescripcionIni"><p> Si estas buscando algun integrante de la Sección informate por aqui
                </p></div>

        </div>
        <div class="col-md-4"><div class="circulo" id="Project">
                <img class="fotoNoso" src="<?php base_url(); ?>assets/img/Proyectos.png"></div>
            <div class="TituloInicio"><h1>Proyectos</h1></div>
            <div class="DescripcionIni"><p>En este lugar podras enterarte de todos los Proyectos realizados 
                    y en proceso que se puede estar realizando en la Sección estudiantil</p></div>

        </div>
    </div>
</section>
<section id="Participar">
    <div id="Even">
        <div class="container" >
            <div id="TituloGeneral"><h1>Como puedes participar</h1></div>
            <div class="col-md-6">
                <div id="unacino">
                    <div id="descIngre">
                        <h2>Perteneces a la Unac:</h2>
                        <ul>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                Ser un actual estudiante de la Unac</li>
                            
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                Tener deseos de trabajar en equipo , aprender y ser
                                agente de cambio para la universidad y la sociedad</li>
                            <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                Pasar una entrevista y un filtro interno en caso de aprobar se le comunicara </li>

                        </ul>
                    </div>
                    <img src="<?php base_url(); ?>assets/img/Unac.png" id="imagenUnac">
                    <div id="pertenecer">
                        <div><h2>Unacinos</h2></div>

                    </div>

                </div>
            </div>

            <div class="col-md-6">
                <div id="descIngre">
                    <h2>Persona Externa:</h2>
                    <ul>
                        <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Registrarte a la Red de Proyectos Unac.</li>
                        <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Se te hara llegar comunicados con los nuevos proyectos y cursos
                            a realizar.</li>
                        <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Tener deseos de aprender y ser
                            agente de cambio en la sociedad.</li>
                        <li><span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                            Cualquier asesoramiento o consultoria no dudes en contactarnos</li>

                    </ul>
                </div>
                <img src="<?php base_url(); ?>assets/img/persona.png" id="imagenPerso">
                <div id="pertenecer">
                    <div><h2>Externos</h2></div>

                </div>

            </div>
        </div>
    </div>
</section>
<section id="Eventos">
    <div id="events">
        <div class="container">
            <div id="TituloGeneral"><h1>Ultimos Eventos Realizados</h1></div>
            <div class="col-md-6">
                <div class="evento">
                    <button id="encabezado" class="btn btn-primary ribbon"> &nbsp;</button>
                    <div id="contenido">
                        <img id="imgevent" src="<?php base_url(); ?>assets/img/eventos/evento2.png"/>

                        <div id="tituloEvento"><h2>Aniversario de Proyectos Unac</h2></div>
                        <div id="DescripcionCurso"><p>El Aniversario de Proyectos Unac se consolido como
                            una reunión de confraternidad por los 12 años de nuestra queridisima seccion
                            estudiantil en la cual estamos totalmente orgullosos de pertenecer.</p></div>
                        
                        
                        <button id="botonEvent" type="button" class="btn btn-primary ribbon">Informarse</button>


                    </div>
                </div>


            </div>
            <div class="col-md-6">
                <div class="evento">
                    <button id="encabezado" class="btn btn-primary ribbon"> &nbsp;</button>
                    <div id="contenido">
                        <img id="imgevent" src="<?php base_url(); ?>assets/img/eventos/evento1.png"/>

                        <div id="tituloEvento"><h2>II Curso de Direccion de Proyectos</h2></div>
                        <div id="DescripcionCurso"><p>El II Curso de Dirección de Proyectos 
                                tiene como objetivo capacitar al alumno para certificarse como 
                                Técnico Certificado en Dirección de Proyectos CAMP®. Conocer todas 
                                las fases, procesos y áreas de la Gestión de Proyectos basados en la 
                                metodología del PMBOK 5ta edición. Promover la capacidad de trabajar 
                                en equipo. Contribuir a la formación del alumno con los conceptos de 
                                Gestión de proyectos</p></div>
                        <button id="botonEvent" type="button" class="btn btn-primary ribbon">Informarse</button>


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>