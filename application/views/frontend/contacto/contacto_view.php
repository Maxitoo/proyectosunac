<article>
    <div id="contacto">
        <div class="col-md-12"><h1>Contactanos</h1></div>
        <div class="col-xs-0 col-md-1"></div> 
        <div class="col-xs-12 col-md-11" id="contactar">

            <div class="col-xs-12 col-sm-6 col-md-6">
                <div ><img class="img-thumbnail" src="<?php base_url(); ?>assets/img/Equipo.jpg" id="EquipoProyectos"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <div>

                    <form action="contacto_view.php" id="enviar">
                        <div class="col-sm-12 col-md-12">
                            <div id="respuesta"></div>
                        </div>
                        <div class="form-group col-sm-12 col-md-12 espacio">
                            <label for="Nombre">Nombre:</label>
                            <div class="input-group">
                                <div class="input-group-addon iconos"><span class="glyphicon glyphicon-user"></span></div>
                                <input type="text" class="form-control" id="Nombre" placeholder="Nombre" required>
                            </div>

                        </div>

                        <div class="form-group col-sm-12 col-md-12 espacio">
                            <label for="Email">E-mail:  </label>
                            <div class="input-group">
                                <div class="input-group-addon iconos"><span class="glyphicon glyphicon-envelope"></span></div>  
                                <input type="email" class="form-control" id="Email" placeholder="ejemplo@ejemplo.com" required>
                            </div>

                        </div>

                        <div class="form-group col-sm-12 col-md-12 espacio">
                            <label for="asunto">Asunto:</label>
                            <div class="input-group">
                                <div class="input-group-addon iconos"><span class="glyphicon glyphicon-text-color"></span></div>
                                <input type="text" class="form-control" id="asunto" placeholder="Asunto" required>
                            </div>

                        </div>
                        <div class="form-group col-sm-12 col-md-12 espacio">
                            <label for="mensaje">Mensaje:</label>
                            <div class="input-group">
                                <div class="input-group-addon iconos"><span class="glyphicon glyphicon-pencil"></span></div>
                                <textarea class="form-control" rows="4"  id="mensaje" required></textarea>
                            </div>

                        </div>
                        <div class="form-group col-sm-12 col-md-12 espacio">
                            <div id="envio" ><input type="submit" id="Enviando" value="Enviar"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-12" id="Oficina">
            <div id="Contactanos">
                <h1>¿Cómo contactarnos?</h1>
                <div class="col-md-6" >
                    <div id="googlemaps"></div>
                    <div id="OpcionesMapa"> 
                        <button type="button" class="btn btn-info" id="llegar">Como llegar</button>
                        <button type="button" class="btn btn-info" id="SituarOf">Situar Oficina</button>
                    </div>

                </div>
                <div class="col-md-6" >
                    <div id="informacionPU">
                        <h3>Oficina Principal</h3>
                        <ul>
                            <li><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>Biblioteca de la Universidad Nacional del Callao,Av Juan Pablo II 306,Bellavista</li>
                            <li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>Proyectos_unac@pmi.org.pe</li>
                           
                        </ul>
                        <div id="posicionarL"><img src="<?php echo base_url();?>assets/img/Logo.png" id="logo"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
