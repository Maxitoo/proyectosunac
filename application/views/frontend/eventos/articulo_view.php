<section>
    <div id="banner-area">
        <img src="<?php echo base_url();?>assets/img/blog.jpg"/>
        <div class="parallax-overlay"></div>
        <div></div>
    </div>
</section>


<section>
    <div id="blog">
        <div class="container">
            <div class="col-sm-12 col-md-12">
                <div class="articulo">
                    <div class="caption">
                        <h3 id="titulo"><?php echo $lista_entradas->titulo; ?></h3>
                        <div id="info_articulo">
                            <img id="foto_autor" src="<?php echo base_url(); ?>assets/img/integrantes/ProyectosUnac.png"/>
                            <div id="autor"><?php echo $lista_entradas->autor; ?></div>
                            <div id="fecha" class="hidden-xs"><?php echo $lista_entradas->fecha; ?></div>
                        </div>
                        <div id="contenido"><p><?php
                                echo $lista_entradas->tags;
                                ?>
                            </p></div>
                    </div>
                </div>
                <div id="comentarios">
                    <h2> Comentarios</h2>
                    <div class="fb-comments" data-href="<?php echo base_url();?>evento/<?php echo $lista_entradas->permalink;?>" data-width="100%" data-numposts="5"></div>
                   
                </div>
            </div>
        </div>
    </div>
</section>