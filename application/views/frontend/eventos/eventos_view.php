

<section>
 <h1 id="titulo4">Eventos</h1>
    <div id="blog">
        <div class="container">
            <?php
            foreach ($lista_entradas as $campo):
                $url = 'evento/' . $campo->permalink;
                ?> 
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="<?php echo base_url() . "assets/img/eventos/" . $campo->imagen; ?>" alt="...">
                        <div class="caption">
                            <h3><?php echo $campo->titulo; ?></h3>
                            <?php echo $campo->fecha; ?>
                            <p><?php
                                echo $campo->tags . "... ";
                                echo anchor($url, "leer mas");
                                ?></p>
                            <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
