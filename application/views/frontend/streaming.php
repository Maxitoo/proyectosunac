<section>
    <div class="container" id="streaming">
        <div class="col-md-6" id="video-youtube"><iframe width="100%" height="400" src="https://www.youtube.com/embed/tI3Gfn8affA" frameborder="0" allowfullscreen></iframe></div>
        <div class="col-md-6" id="chat">
            <button class="btn btn-primary" id="btnchat">Chat</button>
            <button class="btn btn-primary" id="btnpregunta">Formula tu pregunta</button>
            <div id="contenido-chat">
                <?php foreach( $mensajes as $msj){ ?>
                    <p class="mensaje" ><?php echo $msj->m_chat_usuario;
                     echo ": ".$msj->chat_mensaje;?></p>
                <?php }?>
            </div>
            <div id="enviar"><textarea id="enviar_mensajes" name="mensaje" class="form-control" rows="4" style="resize:none"  ></textarea></div>
        </div>
        <div class="col-md-12" id="Preguntas">
            
            <div class="col-md-3"></div>
            <div class="col-md-6" id="Pregunta">
                <form action="<?php base_url(); ?>/streaming/agregar_pregunta" method="post">
                    <div class="form-group" >
                        <label for="exampleInputEmail1">Pregunta:</label>
                        <input name="pregunta_user" type="text" class="form-control" id="exampleInputEmail1" placeholder="Pregunta" required>
                    </div>
                    <button type="submit" id="enviar-pregunta" class="btn btn-default">Enviar</button>
                </form>                
            </div>
            <div class="col-md-3"></div>

        </div>
    </div>
</section>