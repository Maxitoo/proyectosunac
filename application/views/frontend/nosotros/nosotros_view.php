<article >
    <div id="nosotros">
        <div id="get-in-touch">
                <div class="container">
                    <div class="section-header">
                        <h2 class="section-title text-center wow fadeInDown">¿Quiénes Somos?</h2>
                        
                    </div>
                </div>
        </div><!--/#get-in-touch-->
            <div >
                <div class="col-md-12" id="fonda">
                        
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <img src="assets/img/Logo.png">
                    </div>
                    <div class="col-md-8">
                        <p>
                            Nuestra sección estudiantil "Proyectos UNAC" está conformada por estudiantes
                            de diversas facultades de la Universidad Nacional del Callao, siendo así una 
                            asociación estudiantil la cual consiste en difundir, capacitar y asesorar la 
                            cultura de la Dirección de Proyectos bajo el enfoque del Project Management 
                            Institute (PMI) y generar Emprendimiento para el desarrollo institucional en 
                            la sociedad donde nos ubicamos; dirigido a estudiantes universitarios, así como 
                            profesionales, empresarios, empleados y público en general, sección estudiantil la cual 
                            formará parte del Capítulo PMI Lima Perú.
                        </p>
                    </div>
                </div>
                <div  class="col-md-12" id="fond">
                    <div class="col-md-1"></div>
                   
                    <div class="col-md-8">
                        <h1 id="sub">Misión</h1>
                        <h3 id="par">
                            “Somos una institución en la finalidad de promover, difundir, asesorar y formar
                            lideres altamente calificados en dirección de proyectos buscando el desarrollo
                            de la UNAC y de nuestra comunidad”
                        </h3>
                    </div>
                     <div class="col-md-2">
                        <div id="pa">
                            <img  src="assets/img/image/mision.png">
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="col-md-12" id="fondo">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                       <div id="pa">
                            <img  src="assets/img/image/vision.png">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <h1 id="sub">Visión</h1>
                        <h3 id="par">
                        “Una organización estudiantil líder en la cultura de dirección de proyectos para
                        el emprendimiento en el Perú y el mundo”
                        </h3>
                    </div>
                </div>
                 
            </div>
    
                <section id="team">
                        <div id="get-in">
                                <div class="container">
                                            <div class="section-header">
                                                <h2 class="section-title text-center wow fadeInDown">Junta Directiva</h2>
                                            </div>
                                        </div>
                </div><!--/#get-in-touch-->
                    <div class="container" id="te">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                               
                                <div class="team_area">
                                <div class="team_slider">            
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="single_team wow fadeInUp">
                                            <div class="team_img">
                                                <img src="<?php echo base_url();?>assets/img/presidente.jpg" alt="img">
                                            </div>
                                            <h5 class="">Denis Valle Saldivar</h5>
                                            <span>Presidente</span>                        
                                
                                            <div class="team_social">
                                                <a href="https://www.facebook.com/denis.valle.52?fref=ts"><i class="fa fa-facebook"></i></a>
                                                
                                                <a href="https://plus.google.com/117233657866639534463/posts"><i class="fa fa-google-plus"></i></a>
                                                <a href="https://www.linkedin.com/in/denis-valle-saldivar-90a915a6"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="single_team wow fadeInUp">
                                            <div class="team_img">
                                                <img src="<?php echo base_url();?>assets/img/vicepresidente.jpg" alt="img">
                                            </div>
                                            <h5>Diego Enrique Flores Valdivia</h5>
                                            <span>Vicepresidente</span>
                                            <div class="team_social">
                                                <a href="https://www.facebook.com/diego.floresvaldivia?fref=ts"><i class="fa fa-facebook"></i></a>
                                                
                                                <a href="https://plus.google.com/103833909685394236979/posts"><i class="fa fa-google-plus"></i></a>
                                                <a href="https://pe.linkedin.com/in/diego-enrique-flores-valdivia-55644b62"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                        <div class="single_team wow fadeInUp">
                                            <div class="team_img">
                                                <img src="<?php echo base_url();?>assets/img/secretario.jpg" alt="img">
                                            </div>
                                            <h5>Miguel Espinoza Diaz</h5>
                                            <span>Secretario</span>
                                            <div class="team_social">
                                                <a href="https://www.facebook.com/miguelangel.espinozadiaz?fref=ts"><i class="fa fa-facebook"></i></a>
                                                
                                                <a href="https://plus.google.com/107365727615658901494/posts"><i class="fa fa-google-plus"></i></a>
                                                <a href="https://www.linkedin.com/in/miguel-angel-espinoza-diaz-9b2411b3"><i class="fa fa-linkedin"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-4">
                                    <div class="single_team wow fadeInUp">
                                        <div class="team_img">
                                            <img src="<?php echo base_url();?>assets/img/tesorera.jpg" alt="img">
                                        </div>
                                        <h5>Carolina Truevas Malqui</h5>
                                        <span>Tesorera</span>
                                        <div class="team_social">
                                            <a href="https://www.facebook.com/carolina.truevasmallqui?fref=ts"><i class="fa fa-facebook"></i></a>
                                            
                                            <a href="https://plus.google.com/108417221765171595963/posts"><i class="fa fa-google-plus"></i></a>
                                            <a href="https://pe.linkedin.com/in/carolina-truevas-mallqui-62b06897"><i class="fa fa-linkedin"></i></a>
                                        </div>
                                    </div>
                                    </div>
                                </div>                              
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
    </div>
</article>
