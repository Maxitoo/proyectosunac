<article>
    <div id="personas">

        <h1 id="titulo4">Miembros de Proyectos Unac</h1>
        <div class="col-md-1"></div> 
        <div class="col-md-12" id="directivas">
            <div id="prueba">
                
                <div class="col-md-12" id="directivas">
                    <div id="prueba">
                        <div class="col-md-1"></div> 
                        <div class="col-md-2" >
                            <div class="restmenuwrap">
                                <div id="presidente"><img src="<?php echo base_url(); ?>assets/img/Directivas/D_Educacion.jpg"><div class="nombre_D"><h4>Karel Mancha Villa
                                </h4></div></div>
                                <div class="director"><br/>Directora de Educación</div>
                            </div>
                        </div>
                        <div class="col-md-2" >
                            <div class="restmenuwrap">
                                <div id="presidente"><img src="<?php echo base_url(); ?>assets/img/Directivas/D_Tecnologia_Informacion.jpg"><div class="nombre_D"><h4>Yorman Medina Saavedra
                                        </h4></div></div>
                                <div class="director"><br/>Director de Tecnologías de la información</div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="restmenuwrap">
                                <div id="presidente"><img src="<?php echo base_url(); ?>assets/img/Directivas/D_RI.jpg"><div class="nombre_D"><h4>Nelsi Melgarejo Vergara
                                    </h4></div></div>
                            <div class="director"><br/>Directora de Relaciones Institucionales</div>
                            </div>
                        </div>
                        <div class="col-md-2" >
                            <div  class="restmenuwrap">
                                <div id="presidente"><img src="<?php echo base_url(); ?>assets/img/Directivas/D_RH.jpg"><div class="nombre_D"><h4>Adrian Cordova
                                    </h4></div></div>
                            <div class="director"><br/>Director de Recursos Humanos</div>
                            </div>
                        </div>
                        <div class="col-md-2" >
                            <div  class="restmenuwrap">
                                <div id="presidente"><img src="<?php echo base_url(); ?>assets/img/Directivas/D_EyL.jpg"><div class="nombre_D"><h4>Johana Milla
                                    </h4></div></div>
                            <div class="director"><br/>Directora de Economía y Logística</div>
                            </div>
                        </div>
                        <div class="col-md-1"></div> 
                    </div>
                    <div class="col-md-1"></div> 
                </div>
                
                <div class="col-md-12">
                    <div class="col-md-12" id="BQ_integrante">
                        <div class="col-md-4" >
                            <label for="busqueda">Buscar a un integrante de Proyectos Unac:</label>
                        </div>
                        <div class="col-md-4" >
                            <input type="text" class="form-control" id="busqueda" placeholder="Buscar integrante">
                        </div>
                        <div class="col-md-2" >
                            <input type="button" class="btn btn-primary" id="Buscar" value="Buscar" placeholder="Text input">
                        </div>
                    </div>
                    <div class="col-md-12"><div id="Resultado"></div></div>
                    <div class="col-md-12">
                        <div id="contenedorDirectiva">
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="Educacion"> <div id="E">
                                    <img src="<?php echo base_url(); ?>assets/img/educacion.jpg"></div><h4>DIRECTIVA DE EDUACIÓN</h4>  
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="TI"><div id="D_TI">
                                    <img src="<?php echo base_url(); ?>assets/img/TI.jpg"></div><h4>DEPARTAMENTO DE TECNOLOGÍAS DE LA INFORMACIÓN</h4> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="RI"><div id="D_RI">
                                        <img src="<?php echo base_url(); ?>assets/img/RI.jpg"></div><h4>DIRECTIVA DE RELACIONES INSTITUCIONALES</h4> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="CH"><div id="D_CH">
                                        <img src="<?php echo base_url(); ?>assets/img/CH.jpg"></div><h4>DIRECTIVA DE CAPITAL HUMANO</h4> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="EyL"><div id="D_EyL">
                                        <img src="<?php echo base_url(); ?>assets/img/EyL.jpg"></div><h4>DIRECTIVA DE ECONOMÍA Y LOGÍSTICA</h4> 
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="Directiva" data-directiva="PMO"><div id="PMO">
                                        <img src="<?php echo base_url(); ?>assets/img/PMO.jpg"></div><h4>Oficina de Gestión de Proyectos</h4> </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="col-md-1"></div> 
    </div>
</article>
