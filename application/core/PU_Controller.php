<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PU_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function responderJson(array $data) {
        $this->output->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function cargarVista($data) {
        $this->load->view("plantillas/frontend/header", $data);
        $this->load->view("frontend/".$data["view"], $data);
        $this->load->view("plantillas/frontend/footer", $data);
    }

    public function cargarVistaDashboard($data) {
        $this->load->view("layouts/header_dashboard_view", $data);
        $this->load->view($data["view"], $data);
        $this->load->view("layouts/footer_dashboard_view", $data);
    }

    public function subir_imagen($imagen) {

        $path = getcwd();
        $mam = '/cms';
        $pos = strripos($path, $mam);
        $base = substr($path, 0, $pos);
        $target_dir = $base . '/enzenyo_images/bandera/';

        $target_file = $target_dir . basename($imagen["name"]);
        $nameimg = $imagen["name"];
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $mensaje = '';

        // Check if image file is a actual image or fake image
        $check = getimagesize($imagen["tmp_name"]);
        if ($check !== false) {
            // echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            // echo "File is not an image.";
            $uploadOk = 0;
            $mensaje = "El Archivo no es una Imagen";
        }

        // Check if file already exists
        if (file_exists($target_file)) {
            // echo "Sorry, file already exists.";
            $mensaje = "Ya existe un archivo con el mismo nombre.";
            $uploadOk = 0;
        }

        // Check file size
        if ($imagen["size"] > 500000) {
            // echo "Sorry, your file is too large.";
            $mensaje = "La imagen es demasiado grande.";
            $uploadOk = 0;
        }

        // Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            // echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $mensaje = "Solo se permiten archivos JPG, JPEG, PNG & GIF";
            $uploadOk = 0;
        }

        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            // echo "Sorry, your file was not uploaded.";
            $mensaje = "El archivo no pudo ser enviado al servidor.";
            // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($imagen["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
                $mensaje = "La imagen " . basename($imagen["name"]) . " ha sido enviado al servidor correctamente.";
            } else {
                // echo "Sorry, there was an error uploading your file.";
                $mensaje = "Ocurrio un error al momento de enviar la imagen.";
            }
        }

        return array('upload' => $uploadOk, 'path_imagen' => $nameimg, 'mensaje' => $mensaje);
        ;
    }

    protected function initPaginador($config = NULL) {
        $this->load->library('pagination');
        $this->configPaginador['base_url'] = site_url($this->uri->segment(1) . '/' . $this->uri->segment(2));
        $this->configPaginador['per_page'] = PER_PAGE;
        $this->configPaginador['full_tag_open'] = '<div id="paginador">';
        $this->configPaginador['full_tag_close'] = '</div>';
        if ($config !== NULL) {
            foreach ($config as $k => $v) {
                $this->configPaginador[$k] = $v;
            }
        }

        $this->pagination->initialize($this->configPaginador);

        $this->paginadorPerPage = $this->configPaginador['per_page'];
    }

}
